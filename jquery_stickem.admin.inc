<?php

/**
 * @file
 * Administrative functions for the jquery_stickem module.
 */

/**
 * Administration form for jquery_stickem.
 *
 * @return array
 *   Array of form elements.
 */
function jquery_stickem_admin_form() {
  $form = array();

  $form['jquery_stickem_selectors'] = array(
    '#type' => 'textarea',
    '#title' => t('Selectors'),
    '#default_value' => variable_get('jquery_stickem_selectors', ''),
    '#description' => t("Selectors to which the stickem() method should be applied. One per line."),
  );

  $form['jquery_stickem_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths'),
    '#default_value' => variable_get('jquery_stickem_pages', ''),
    '#description' => t("Pages where the stickem() code should be included. Enter one path per line. The '*' character is a wildcard. If empty will be included on all pages."),
  );

  $default_css_path = drupal_get_path('module', 'jquery_stickem') . '/css/jquery_stickem.css';
  $css_path = variable_get('jquery_stickem_css_path', $default_css_path);
  $form['jquery_stickem_css_path'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS Path'),
    '#default_value' => $css_path,
    '#description' => t("Path to the CSS file."),
  );

  $form['stickem_defaults'] = array(
    '#type' => 'fieldset',
    '#title' => t("Override Stick 'em default settings"),
    '#description' => t('See documentation here: !stickem_link', array('!stickem_link' => l(t("Stick 'em github page"), 'https://github.com/davist11/jQuery-Stickem'))),
  );

  $form['stickem_defaults']['jquery_stickem_item'] = array(
    '#type' => 'textfield',
    '#title' => t('Item'),
    '#default_value' => variable_get('jquery_stickem_item', JQUERY_STICKEM_ITEM_DEFAULT),
    '#description' => t("Class name of the element which should be 'stuck'"),
  );

  $form['stickem_defaults']['jquery_stickem_container'] = array(
    '#type' => 'textfield',
    '#title' => t('Container'),
    '#default_value' => variable_get('jquery_stickem_container', JQUERY_STICKEM_CONTAINER_DEFAULT),
    '#description' => t("Class name of the containing element"),
  );

  $form['stickem_defaults']['jquery_stickem_stickclass'] = array(
    '#type' => 'textfield',
    '#title' => t('StickClass'),
    '#default_value' => variable_get('jquery_stickem_stickclass', JQUERY_STICKEM_STICKCLASS_DEFAULT),
    '#description' => t("Class name which should be applied when the element to be stuck reaches the top of the viewport. Do not include the dot."),
  );

  $form['stickem_defaults']['jquery_stickem_endstickclass'] = array(
    '#type' => 'textfield',
    '#title' => t('EndStickClass'),
    '#default_value' => variable_get('jquery_stickem_endstickclass', JQUERY_STICKEM_ENDSTICKCLASS_DEFAULT),
    '#description' => t("Class name which should be applied when the element to be stuck reaches the bottom of the containing element. Do not include the dot."),
  );

  $form['stickem_defaults']['jquery_stickem_offset'] = array(
    '#type' => 'textfield',
    '#title' => t('Offset'),
    '#default_value' => variable_get('jquery_stickem_offset', JQUERY_STICKEM_OFFSET_DEFAULT),
    '#description' => t("Number of pixels offset which should be applied to the stuck element."),
  );

  $form['stickem_defaults']['jquery_stickem_start'] = array(
    '#type' => 'textfield',
    '#title' => t('Start'),
    '#default_value' => variable_get('jquery_stickem_start', JQUERY_STICKEM_START_DEFAULT),
    '#description' => t("Number of pixels after which the stickem class should be applied to the stuck element."),
  );

  return system_settings_form($form);
}
