# About
The jquery_stickem module provides the necessary functionality to include the jQuery Stick ’em library and provides basic configuration allowing you to specify the paths and selectors to which it should be applied and override the default parameters.

Details on the jQuery Stick ’em library are available here.
https://www.viget.com/articles/jquery-stick-em

*Note I am not the author of the original library.*

# Installation
Download the jquery_stickem module and dependencies (libraries and jquery_update).

The library requires a minimum of jQuery 1.7, so use the jquery_update configuration to set the default jQuery version to at least 1.7

Download the library from github:
https://github.com/davist11/jQuery-Stickem

And upload to your libraries directory into a directory called 'stickem'. eg the path to the library should be:
sites/all/libraries/stickem/jquery.stickem.js

Enable the jquery_stickem module.

# Usage
The module itself simply enables you to include the library on various pages, and specify some defaults.

As the library is mostly dependent on styles to control the positioning of certain elements you will still need to ensure your markup and styles work together accordingly.

Check the Usage and Demo to see examples of how things should be put together
https://github.com/davist11/jQuery-Stickem
https://davist11.github.io/jQuery-Stickem/

Note that if you change the default classes you will need to recreate the styles in your theme.
