You can use this form to specify selectors and paths to which stickem should be applied, and override the stick 'em defaults. If a stick 'em default setting is overridden the overrides will need to be styled accordingly.

If Paths is empty the library and css will be included on every page.

If using different class names you can specify a custom css path.

Refer to the github page and demo for styling and implementation examples:
https://github.com/davist11/jQuery-Stickem
https://davist11.github.io/jQuery-Stickem/

If you need to override the defaults for a specific path you can do this as a pipe delimited list, in the form:
path|Item|Container|StickClass|EndStickClass|Offset|Start
eg:
node/*|.stickme|.stickme-container|stuck|unstuck|50|40